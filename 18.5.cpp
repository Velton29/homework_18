﻿#include <iostream>
#include "stack.h"

int main()
{
    int q;
    std::cout << "How many characters to work with?" << '\n';

    std::cin >> q;

    Stack<char> stackSymbol(q);
    int ct = 0;
    char ch;

    std::cout << '\n' << "Write " << q << " simbols" << '\n';

    while (ct++ < q)
    {
        std::cin >>  ch;
        stackSymbol.push(ch);
    }

    std::cout << std::endl;

    stackSymbol.printStack();

    std::cout << '\n' << '\n' << "Delete element from stek" << '\n';
    stackSymbol.pop();

    stackSymbol.printStack();

    Stack<char> newStack(stackSymbol);

    std::cout << '\n' << '\n' << "Copy constructor worked" << '\n';
    newStack.printStack();

    int x;
    std::cout << '\n' << "Which element to output?" << '\n';
    std::cin >> x;

    std::cout << '\n' << x << ' ' << "element in the queue: " << '\n' << newStack.Peek(x) << std::endl;

    return 0;
}